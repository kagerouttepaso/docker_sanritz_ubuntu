FROM ubuntu:14.04
MAINTAINER yuki kitsunai <goth.wasawasa@gmail.com>

#add env_keep
RUN echo 'Defaults        env_keep="http_proxy https_proxy ftp_proxy no_proxy"' >> /etc/sudoers

#apt setting
RUN sed -i -e"s;http://;http://jp.;g" /etc/apt/sources.list && \
      apt-get update && \
      apt-get install -y curl wget ruby ruby-dev g++ make gcc

#install chef
RUN curl -s https://www.getchef.com/chef/install.sh | sudo bash

#install gems
RUN mkdir /chef
ADD ./chef /chef
RUN cd /chef && \
      gem install bundler --no-ri --no-rdoc && \
      bundle install
#      librarian-chef install && \
#      chef-solo -j ./nodes/localhost.json -c solo.rb



VOLUME ["/kitchen"]

#proxy setting
ENV http_proxy  http://172.16.2.9:80
ENV https_proxy http://172.16.2.9:80
ENV ftp_proxy   http://172.16.2.9:80

RUN echo 'http_proxy=''"'$http_proxy'"'  >> /etc/environment
RUN echo 'https_proxy=''"'$http_proxy'"' >> /etc/environment
RUN echo 'ftp_proxy=''"'$http_proxy'"'   >> /etc/environment

